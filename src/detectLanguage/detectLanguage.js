"use strict";

function detectLanguage(allowlangArray, selectLang  ) {

    if ( !Array.isArray(allowlangArray) || allowlangArray.length == 0) {
        throw new Error("given empty array, or not array");
    }

    else if ( selectLang ) {
        return selectLang;
    }

    else if ( allowlangArray.indexOf(detectBrowserLanguage()) != -1 ){      
        return  detectBrowserLanguage();
    }

    else {
        return allowlangArray[0]
    }

    }

function detectBrowserLanguage(){
    let locale = (navigator.languages && navigator.languages[0].split(/[-_]/)[0]) || navigator.language.split(/[-_]/)[0] || navigator.userLanguage.split(/[-_]/)[0];
    locale.toLowerCase();
    return locale;
}

module.exports = { detectLanguage };

