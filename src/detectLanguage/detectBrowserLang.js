function detectBrowserLanguage(){

    const locale = (navigator.languages && navigator.languages[0]) || navigator.language || navigator.userLanguage;
    locale.split(/[-_]/)[0];
    return locale.toLowerCase();

}

module.exports = { detectBrowserLanguageAndGetToLowChase: detectBrowserLanguage };