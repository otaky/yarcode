"use strict";

const { detectLanguage } = require('./detectLanguage');


describe('DetectLanguage', () => {


    it('[] => error ]', () => {
        expect(() => detectLanguage([] ) ).toThrow(new Error("given empty array, or not array"));
    });

    it('{} => error', () => {
        expect(() => detectLanguage({} ) ).toThrow(new Error("given empty array, or not array"));
    });

    it('sting => error', () => {
        expect(() => detectLanguage("sdf" ) ).toThrow(new Error("given empty array, or not array"));
    });

    it('number => error', () => {
        expect(() => detectLanguage(123 ) ).toThrow(new Error("given empty array, or not array"));
    });

    it('selectLang="su" => return "su" return supported lang', () => {
        expect(  detectLanguage(["en", "ru", "de"], "su" ) ).toEqual("su");
    });

    it('locale="fn" => return "en" "fn" not supported', () => {
        navigator.__defineGetter__('language', function(){
            return 'fn';
        });
        expect(  detectLanguage(["en", "ru", "de"] ,  ) ).toEqual("en");
    });

    it('locale="ru" => return "ru" return supported lang', () => {
        navigator.__defineGetter__('languages', function(){
            return ["ru-RU", "ru"];
        });
        expect(  detectLanguage(["en", "ru", "de"] ) ).toEqual("ru");
    });

    it('locale="undefined" => return "en" return supported lang', () => {
        navigator.__defineGetter__('languages', function(){
            return undefined;
        });
        expect(  detectLanguage(["en", "ru", "de"] ) ).toEqual("en");
    });

});
